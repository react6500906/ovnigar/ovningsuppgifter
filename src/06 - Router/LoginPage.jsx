import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function LoginPage() {
  const [name, setName] = useState("");
  const navigate = useNavigate();

  function handleLogin() {
    navigate("/landing", { state: { name } });
  }

  return (
    <div>
      <h1>Logga in</h1>
      <input
        type="text"
        name="name"
        vlaue={name}
        placeholder="Användarnamn"
        onChange={(e) => setName(e.target.value)}
      />
      <button onClick={handleLogin}>Login</button>
    </div>
  );
}

export default LoginPage;
