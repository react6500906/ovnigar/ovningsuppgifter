import React from 'react'
import { useLocation } from 'react-router-dom'

function LandingPage() {
const location = useLocation()
const {name} = location.state

  return (
    <h1>Hej {name}</h1>
  )
}

export default LandingPage