import React, { useState } from 'react'

function DarkLightToggle() {
    const [darkMode, setDarkMode] = useState(false)

const themeStyle= {
    backgroundColor: darkMode ? "#333" : "#FFF",
    color: darkMode ? "white" : "black",
    width: "400px",
    height: "400px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
}

function toggleMode(){
    setDarkMode(!darkMode)
}

  return (
    <div style={themeStyle}>
        <button onClick={toggleMode}>
            Byt färgtema
        </button>
        <p>Vilken färg är jag?</p>
    </div>
  )
}

export default DarkLightToggle