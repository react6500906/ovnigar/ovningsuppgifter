import React from "react";
import image1 from "./../assets/flower.jpg";
import image2 from "./../assets/stones.jpg";
import { useState } from "react";

function ImageToggle() {
  const [currentImage, setCurrentImage] = useState(image1);

  function toggleImage() {
    setCurrentImage(currentImage === image1 ? image2 : image1);
  }

  return (
    <div>
      <img src={currentImage} alt="" style={{ width: "25%" }} />
      <button onClick={toggleImage}>Byt bild</button>
    </div>
  );
}

export default ImageToggle;
