import React from "react";

function StyledParagraph() {
    
const pStyle = { color: "blue", fontSize: "20px", fontFamily: "Helvetica" }
const text = "Min stylade paragraf"

  return (
    <div>
      <p style={pStyle}>
        {text}
      </p>
    </div>
  );
}

export default StyledParagraph;
