import React, {useState} from 'react'

function CounterButton() {
const [count, setCount] = useState(0)

function increment(){
setCount(count +1)
}

  return (
    <div>
        <button onClick={increment}>Antal klick: {count}</button>
    </div>
  )
}

export default CounterButton