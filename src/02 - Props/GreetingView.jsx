import React from "react";

function GreetingView() {
  const [name, setName] = useState("Ahmed");

  return (
    <>
      <Greeting name={name}></Greeting>
      <Button setName={setName}></Button>
    </>
  );
}

export default GreetingView;

function Greeting({ name }) {
  return <div>Hej {name}</div>;
}

function Button({ setName }) {
  return (
    <>
      <button onClick={() => setName("Richard")}>Byt namn</button>
    </>
  );
}
