import React, { useState } from "react";
import image from "./../assets/flower.jpg"

const info = {
  name: "Ysme",
  age: 30,
  city: "Bangladesh",
  image: image,
};

function ProfileView() {
  const [user, setUser] = useState(info);
  return <UserProfile user={user}></UserProfile>;
}

export default ProfileView;

function UserProfile({ user }) {
  return (
    <>
      <div className="card">
        <div className="avatar-container">
          <img src={user.image} alt="avatar" className="avatar" />
        </div>
        <div className="info">
          <h2 className="name">{user.name}</h2>
          <p>{user.age} år</p>
        </div>
        <div className="details">
          <p>{user.city}</p>
        </div>
      </div>
    </>
  );
}
