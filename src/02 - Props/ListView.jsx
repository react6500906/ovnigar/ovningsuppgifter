import React from "react";

function ListView() {
  let fruits = [
    {
      id: 1,
      name: "appelsin",
    },
    {
      id: 2,
      name: "äpple",
    },
    {
      id: 3,
      name: "kiwi",
    },
    {
      id: 4,
      name: "banan",
    },
    {
      id: 5,
      name: "jordgubbe",
    },
  ];

  return <ItemList fruits={fruits} />;
}

export default ListView;

function ItemList({ fruits }) {
  return (
    <>
      <ul>
        {fruits.map((fruit) => (
          <li key={fruit.id}>{fruit.name}</li>
        ))}
      </ul>
    </>
  );
}
