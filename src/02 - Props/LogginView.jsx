import React from 'react'

function LogginView() {
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    function handleLogIn(){
      setIsLoggedIn(!isLoggedIn)
    }
  
    function handleLogOut(){
      setIsLoggedIn(!isLoggedIn)
    }
  
    return (
      <>
        {isLoggedIn ? (
          <Button text="Logga ut" onClick={handleLogIn}></Button>
        ) : (
          <Button text="Logga in" onClick={handleLogOut}></Button>
        )}
      </>
    );
  }

export default LogginView


function Button({text, onClick}) {
    return (
      <button onClick={onClick}>{text}</button>
    )
  }