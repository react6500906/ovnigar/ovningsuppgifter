import BlogPost from "./BlogPost";
import React, { useState, useEffect } from "react";
import BlogPostForm from "./BlogPostForm";
import axios from "axios";



function Blog() {
  const [blogPosts, setBlogPosts] = useState([]);



  useEffect(() => {
    fetchPosts();
  }, []);

  
  const fetchPosts = async () => {
    try {
      const response = await axios.get("http://localhost:3000/posts");
      setBlogPosts(response.data);
    } catch (error) {
      console.error("Failed to fetch blog posts:", error);
    }
  };



  const deletePost = async (id) => {
    try {
      await axios.delete(`http://localhost:3000/posts/${id}`);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <h2>Bloggposter</h2>
      <BlogPostForm setBlogPosts={setBlogPosts}></BlogPostForm>
      <section className="posts">
        <ul>
          {blogPosts.map((post) => (
            <BlogPost
              key={post.id}
              post={post}
              deletePost={deletePost}
            ></BlogPost>
          ))}
        </ul>
      </section>
    </div>
  );
}

export default Blog;
