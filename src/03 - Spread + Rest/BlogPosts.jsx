import React, { useState } from "react";

const blogPosts  = [
  { id: 1, title: "First Post", content: "This is the first blog post." },
  { id: 2, title: "Second Post", content: "This is the second blog post." },
  { id: 3, title: "Third Post", content: "This is the third blog post." },
  { id: 4, title: "Fourth Post", content: "This is the fourth blog post." },
  { id: 5, title: "Fifth Post", content: "This is the fifth blog post." },
];

const BlogPosts = () => {
  const [posts, setPosts] = useState(blogPosts);

  const handleAddPost = () => {
    let newPost = {
      id: posts.length + 1,
      title: `New post`,
      content: `This is the newest blog post.`,
    };
    setPosts(prev => [...prev, newPost]);
  };

  return (
    <article>
      {posts.map((post) => (
        <div key={post.id}>
          <h2>{post.title}</h2>
          <p>{post.content}</p>
        </div>
      ))}
      <button onClick={handleAddPost}>Add New Post</button>
    </article>
  );
};

export default BlogPosts;
