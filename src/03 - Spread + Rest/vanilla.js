/* Rest */

function skrivUtArgument(...args) {
  args.forEach((arg) => console.log(arg));
}

skrivUtArgument(1, 2, 3, "fyra", "fem"); 


/* Spread */

const lista1 = [1, 2, 3];
const lista2 = [4, 5, 6];


const kombineradLista = [...lista1, ...lista2];
console.log(kombineradLista); // [1, 2, 3, 4, 5, 6]


const lista3 = ["start", ...lista1, "mellan", ...lista2, "slut"];
console.log(lista3); // ['start', 1, 2, 3, 'mellan', 4, 5, 6, 'slut']
