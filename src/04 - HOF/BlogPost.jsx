export default function BlogPost({ post, deletePost }) {
    return (
      <>
        <li>
          <h3>{post.title}</h3>
          <p>{post.content}</p>
          <button onClick={() => deletePost(post.id)}>Ta bort</button>
        </li>
      </>
    );
  }