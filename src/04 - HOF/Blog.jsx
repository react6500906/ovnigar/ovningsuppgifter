import BlogPost from "./BlogPost";
import React, { useState } from "react";

const data = [
    {
      id: 1,
      title: "Resa till Island",
      content: "En underbar resa till Island...",
    },
    {
      id: 2,
      title: "Matlagning med grönsaker",
      content: "Grönsaker är inte bara hälsosamma, de är också...",
    },
    {
      id: 3,
      title: "Teknikens under",
      content: "I den moderna världen är teknik överallt...",
    },
    {
      id: 4,
      title: "En dag i naturen",
      content: "Att tillbringa en dag i naturen kan vara otroligt avkopplande...",
    },
  ];

  

function Blog() {

    const [blogPosts, setBlogPosts] = useState(data);

    function addPost() {
      const newPost = {
        id: Math.floor(Math.random() * 1000),
        title: "Inlägg",
        content: "Nytt inlägg med info",
      };
      setBlogPosts((prevState) => [...prevState, newPost]);
    }
  
    function deletePost(id) {
      setBlogPosts((prevState) => prevState.filter((post) => post.id !== id));
    }

  return (
    <div>
      <h2>Bloggposter</h2>
      <button onClick={addPost}>Skapa inlägg</button>
      <ul>
        {blogPosts.map((post) => (
          <BlogPost
            key={post.id}
            post={post}
            deletePost={deletePost}
          ></BlogPost>
        ))}
      </ul>
    </div>
  );
}

export default Blog;
