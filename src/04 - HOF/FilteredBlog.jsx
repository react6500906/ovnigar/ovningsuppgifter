import BlogPost from "./BlogPost";
import React, { useState } from "react";

const data = [
    {
      id: 1,
      title: "Resa till Island",
      content: "En underbar resa till Island...",
    },
    {
      id: 2,
      title: "Matlagning med grönsaker",
      content: "Grönsaker är inte bara hälsosamma, de är också...",
    },
    {
      id: 3,
      title: "Teknikens under",
      content: "I den moderna världen är teknik överallt...",
    },
    {
      id: 4,
      title: "En dag i naturen",
      content: "Att tillbringa en dag i naturen kan vara otroligt avkopplande...",
    },
  ];


export default function FilteredBlog() {
  const [blogPosts, setBlogPosts] = useState(data);
  const [searchTerm, setSearchTerm] = useState("");


  function deletePost(id) {
    setBlogPosts((prevState) => prevState.filter((post) => post.id !== id));
  }

  const filteredPosts = blogPosts.filter((post) =>
    post.title.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div>
      <h2>Bloggposter</h2>
      <input
        type="text"
        placeholder="Sök efter titel"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <ul>
        {filteredPosts.map((post) => (
          <BlogPost key={post.id} post={post} deletePost={deletePost} />
        ))}
      </ul>
    </div>
  );
}
