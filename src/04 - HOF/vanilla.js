/* NIVÅ 1 */

// 1
let numbers = [4, 42, 17, 2, 765, 32, 21, 5, 754, 3];

// 2
let names = ['Youssef', 'Kamilla', 'Arne', 'Björn', 'Olof', 'Meister', 'Seven', 'Anton', 'Saad', 'Awatif'];

// 3
let employees = [
    { name: 'Youssef', age: 31, salary: 50000, gender: 'Male' },
    { name: 'Kamilla', age: 35, salary: 60000, gender: 'Female' },
    { name: 'Olof', age: 41, salary: 55000, gender: 'Male' },
    { name: 'Anna', age: 28, salary: 58000, gender: 'Female' },
    { name: 'Benita', age: 31, salary: 50000, gender: 'Female' }
]

// 4
let newNumbers = numbers.filter(num => num > 20);
console.log(newNumbers);

// 5
let newerNumbers = numbers.map(num => num * 2);
console.log(newerNumbers);

// 6
let sortNumbers = numbers.sort((n, m) => n - m);
console.log("6" + sortNumbers);

// 7
let otherSort = numbers.slice().sort((n, m) => m - n);
console.log(otherSort);

// 8
let sumNumbers = numbers.reduce((n, m) => n + m);
console.log(sumNumbers);

// 9
numbers.forEach(n => console.log(n));

// 10
let filterNames = names.filter(n => n.startsWith('S'));
console.log(filterNames);

// 11
let upperNames = names.map(n => n.toUpperCase());
console.log(upperNames);

// 12
let sortNames = names.slice().sort();
console.log(sortNames);

// 13
let longName = names.reduce((n, m) => n + ', ' + m);
console.log(longName);

// 14
let nameLength = names.map(n => n.length);
console.log(nameLength);

// 15
let sortFilterObjects = employees.filter(n => n.gender === 'Female')
    .sort((n, m) => m.salary - n.salary);
console.log(sortFilterObjects);

/* NIVÅ 2 */

// 1
let totalAge = employees
    .map(n => n.age)
    .reduce((n, m) => n + m);

console.log(totalAge);

// 2
let maleSalary = employees
    .filter(n => n.gender == 'Male')
    .map(s => s.salary)
    .reduce((n,m, _, arr) => (n + m) / arr.length);

console.log(maleSalary);

// 3
let sortLetters = employees.slice()
    .sort((n,m) => n.name.length - m.name.length);

console.log(sortLetters);

/* NIVÅ 3 */

// 4
const empId = employees
    .map((emp, index) => ({ id: index + 1, ...emp}));

console.log(empId);

// 5
const antal = employees
    .map(emp => emp.gender === 'Female' ? 1 : 0)
    .reduce((n, m, _, arr) => n + m / arr.length);

console.log((procent * 100) + '%');

// 6
const evenOdd = numbers
    .reduce((prev, curr) => {
        curr % 2 === 0 ? prev.even.push(curr) : prev.odd.push(curr)
        return prev;
    }, {odd: [], even: [] });

console.log(evenOdd);

// 7
const isPrime = (num) => {
    if(num < 2) return false;
    for (let i = 0; i < num; i++) {
        if (num % i == 0) return false;
    }
    return true;
}

const primeNums = numbers
    .filter(isPrime);

console.log(primeNums);
