import React, { useState } from "react";

const data = [
  { id: 3, name: "T-shirt", price: 200 },
  { id: 9, name: "Jeans", price: 400 },
];

export default function Cart() {
  const [cartItems, setCartItems] = useState(data);

  const totalCost = cartItems.reduce((total, item) => total + item.price, 0);
  return (
    <>
      <div>
        <h2>Kundvagn</h2>
        <ul>
          {cartItems.map((item) => (
            <CartItem key={item.id} item={item} />
          ))}
        </ul>
        <p>Total kostnad: {totalCost} kr</p>
      </div>
    </>
  );
}

function CartItem({ item }) {
  return <li>{item.name} - {item.price} kr</li>;
}

