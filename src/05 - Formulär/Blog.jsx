import BlogPost from "./BlogPost";
import React, { useState } from "react";
import BlogPostForm from "./BlogPostForm";

const data = [
  {
    id: 1,
    title: "Resa till Island",
    content: "En underbar resa till Island...",
  },
  {
    id: 2,
    title: "Matlagning med grönsaker",
    content: "Grönsaker är inte bara hälsosamma, de är också...",
  },
  {
    id: 3,
    title: "Teknikens under",
    content: "I den moderna världen är teknik överallt...",
  },
  {
    id: 4,
    title: "En dag i naturen",
    content: "Att tillbringa en dag i naturen kan vara otroligt avkopplande...",
  },
];

function Blog() {
  const [blogPosts, setBlogPosts] = useState(data);


  function deletePost(id) {
    setBlogPosts((prevState) => prevState.filter((post) => post.id !== id));
  }

  return (
    <div>
      <h2>Bloggposter</h2>
      <BlogPostForm setBlogPosts={setBlogPosts}></BlogPostForm>
      <section className="posts">
        <ul>
          {blogPosts.map((post) => (
            <BlogPost
              key={post.id}
              post={post}
              deletePost={deletePost}
            ></BlogPost>
          ))}
        </ul>
      </section>
    </div>
  );
}

export default Blog;
