import React, { useState } from "react";

function BlogPostForm({ setBlogPosts }) {
  const [newPost, setNewPost] = useState({ title: "", content: "" });
  const [formErorrs, setFormErrors] = useState({ title: "", content: "" });

  const MAX_TITLE_LENGHT = 10;
  const MAX_CONTENT_LENGT = 150;

  function handleChange(e) {
    const { name, value } = e.target;
    let error = "";

    if (name === "title" && value.length > MAX_TITLE_LENGHT) {
      error = "Din rubrik har för många tecken";
    } else if (name === "content" && value.length > MAX_CONTENT_LENGT) {
      error = "Innhållstexten är för lång";
    } else {
      error = "";
    }

    setNewPost({ ...newPost, [name]: value });
    setFormErrors({ ...formErorrs, [name]: error });
  }

  function handleSubmit(e) {
    e.preventDefault();

    const postToAdd = {
      ...newPost,
      id: Math.floor(Math.random() * 1000),
      date: new Date().toLocaleDateString("sv-SE", {
        year: "numeric",
        month: "long",
        day: "numeric",
      }),
    };

    setBlogPosts((prevState) => [...prevState, postToAdd]);
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div className="body">
          <div className="fileds">
            <input
              type="text"
              name="title"
              value={newPost.title}
              placeholder="Title..."
              onChange={handleChange}
            />
            {formErorrs.title && (
              <div style={{ color: "red" }}>{formErorrs.title}</div>
            )}
          </div>
          <textarea
            name="content"
            value={newPost.content}
            placeholder="Inehåll.."
            onChange={handleChange}
          ></textarea>
          {formErorrs.content && (
            <div style={{ color: "red" }}>{formErorrs.content}</div>
          )}
        </div>
        <button type="submit">Skapa inlägg</button>
      </form>
    </div>
  );
}

export default BlogPostForm;
