import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import LoginPage from "./06 - Router/LoginPage";
import LandingPage from "./06 - Router/LandingPage";
import PageNotFound from "./06 - Router/PageNotFound";
import Blog from "./07 - Axios/Blog";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Blog></Blog>}></Route>
          <Route path="/landing" element={<LandingPage></LandingPage>}></Route>
          <Route path="/*" element={<PageNotFound></PageNotFound>}/>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
